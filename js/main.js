$(document).ready(function(){

        $('.menu_burger').click(function(e) {
            e.preventDefault();
            $(this).next().slideToggle();
        });

    /*====================*/

        $('.search_icon').click(function(e) {
            e.preventDefault();
            $(this).next().slideToggle();
        });

    /*====================*/

        $('.sb_vertical_carousel').slick({
            dots: true,
            arrows: false,
            rows: 3,
        });

    /*====================*/

        $('ul.list.sidebar_menu li.menu-item-has-children.current-menu-item, ul.list.sidebar_menu li.menu-item-has-children.current-menu-parent').addClass('open');
        $('ul.list.sidebar_menu li.menu-item-has-children').append('<span class="switch"></span>');

        $('ul.list.sidebar_menu li.menu-item-has-children > span.switch, ul.list.sidebar_menu li.menu-item-has-children > a.dropdown-toggle').click(function(e) {
            e.preventDefault();
            $(this).toggleClass('open');
            $(this).parent().find('.sub-menu').slideToggle();
        });

    /*====================*/

        $('.persons_list').slick({
            dots: false,
            arrows: true,
            rows: 6,
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        rows: 4
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        rows: 2
                    }
                }
            ]
        });

    /*====================*/

        $('.main_slider').slick({
            dots: true,
            arrows: false,
            rows: 1,
            autoplay: true,
            autoplaySpeed: 5000,
        });

    /*====================*/

        $('.news_post_slider1').slick({
            dots: false,
            arrows: false,
            rows: 1,
            fade: true,
            asNavFor: '.news_post_slider2'
        });

        $('.news_post_slider2').slick({
            dots: false,
            infinite: true,
            arrows: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: '.news_post_slider1',
            focusOnSelect: true
        });

    /*====================*/

        $('.catalog_post_slider1').slick({
            dots: false,
            arrows: false,
            rows: 1,
            fade: true,
            asNavFor: '.catalog_post_slider2'
        });

        $('.catalog_post_slider2').slick({
            dots: false,
            infinite: true,
            arrows: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            asNavFor: '.catalog_post_slider1',
            focusOnSelect: true
        });

    /*====================*/

        $('.consultation_btn').click(function (e) {
            e.preventDefault();
            $('#consultation').arcticmodal();
        });

    /*====================*/

        $('#reviews_carousel').slick({
            dots: false,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });

    /*====================*/

        $('#logos_carousel').slick({
            dots: false,
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 4,
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });

    /*====================*/

        $('#news_carousel').slick({
            dots: false,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });

    /*====================*/

        $('ul.tabs__caption').on('click', 'li:not(.active)', function() {
            $(this).addClass('active').siblings().removeClass('active')
                .closest('div.catalog_tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
        });

    /*====================*/

        $('.showhide_btn').click(function(e) {
            e.preventDefault();
            $(this).toggleClass('active');;
            $(this).parent().find('.short_block').slideToggle();
            $(this).parent().find('.full_block').slideToggle();
        });

    /*====================*/

        $('.showhide_filter_btn').click(function(e) {
            e.preventDefault();
            $(this).toggleClass('active');;
            $(this).next().slideToggle();
        });

    /*====================*/

        $("#filter_range").slider({
            animate: "slow",
            range: true,
            values: [ 10, 65 ],
            slide : function(event, ui) {
                $("#filter_range .ui-slider-handle").text( "от " + ui.values[ 0 ] + " до " + ui.values[ 1 ] );

                $('#filter_range .ui-slider-handle').each(function(i) {
                    $(this).html( "<span>" + ui.values[ i ] + "</span>" );
                });
            }
        });
        $('#filter_range .ui-slider-handle').each(function(i) {
            $(this).html( "<span>" + $("#filter_range").slider("values", i) + "</span>" );
        });

    /*====================*/

        if ( $( ".scrollIn.onlyY" ).length ) {
            const psY = new PerfectScrollbar('.scrollIn.onlyY', {
                suppressScrollX: true
            });
        }
        if ( $( ".scrollIn.bothXY" ).length ) {
            const psXY = new PerfectScrollbar('.scrollIn.bothXY');
        }

    /*====================*/

        var diplomas__button = $('.diplomas__images .pic');
        var diplomas__uploader = $('<input type="file" accept="image/*" />');
        var diplomas__images = $('.diplomas__images');
        var diplomas__num = 0;

        diplomas__button.on('click', function () {
            diplomas__num++;
            diplomas__uploader.click()
        })

        diplomas__uploader.on('change', function () {
            var reader = new FileReader()
            reader.onload = function(event) {
                diplomas__images.prepend('<div class="img" style="background-image: url(\'' + event.target.result + '\');" rel="'+ event.target.result  +'"><span>x</span><input type="hidden" name="diplomas_' + diplomas__num + '" value="' + event.target.result + '" /></div>')
            }
            reader.readAsDataURL(diplomas__uploader[0].files[0])

            if (diplomas__num >= 4) {
                diplomas__button.remove();
            }
        })

        diplomas__images.on('click', '.img', function () {
            $(this).remove()
        })

    /*====================*/

        var gallery__button = $('.gallery__images .pic');
        var gallery__uploader = $('<input type="file" accept="image/*" />');
        var gallery__images = $('.gallery__images');
        var gallery__num = 0;

        gallery__button.on('click', function () {
            gallery__num++;
            gallery__uploader.click()
        })

        gallery__uploader.on('change', function () {
            var reader = new FileReader()
            reader.onload = function(event) {
                gallery__images.prepend('<div class="img" style="background-image: url(\'' + event.target.result + '\');" rel="'+ event.target.result  +'"><span>x</span><input type="hidden" name="gallery_' + gallery__num + '" value="' + event.target.result + '" /></div>')
            }
            reader.readAsDataURL(gallery__uploader[0].files[0])

            if (gallery__num >= 4) {
                gallery__button.remove();
            }
        })

        gallery__images.on('click', '.img', function () {
            $(this).remove()
        })

    /*====================*/

        $(".user_select").chosen({
            disable_search: true
        });
});